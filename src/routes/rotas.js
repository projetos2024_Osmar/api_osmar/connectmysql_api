
const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require('../controller/clienteController');
const pizzariaController = require('../controller/pizzariaController');

router.get("/tables", dbController.getTables); // Consulta tabelas do banco

router.post("/cliente", clienteController.createCliente); // Cadastro de clientes

router.get("/clientes", clienteController.getAllClientes); // Consulta para select da tabela cliente

router.get("/clientes2", clienteController.getAllClientes2); // Consulta para select da tabela cliente com filtro

//rota para listar pizzas

router.get("/pedidosPizzas", pizzariaController.getPedidos);
router.get("/pedidosPizzasJoin", pizzariaController.getPedidosComJoin);

module.exports = router;