const connect = require("../db/connect");

module.exports = class clienteController{
    static async createCliente(req, res){
        const{
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia
        } = req.body;
        
        if (telefone !== 0){
            const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento,  bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
            )`;

            try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Usúario não cadastrado no banco!" });
            return;
          }
          console.log("Inserido no banco!");
          res.status(201).json({ message: "Usúario criado com sucesso" });
        });
      } catch (error) {
        console.error("Erro ao executar o insert! : ", error);
        res.status(500).json({ error: "Erro interno do servidor!" });
      }
    }
    else{
        res.status(400).json({message: "O telefone é obrigatório!!"});
    }
        }
        
        static async getAllClientes(req, res){
          const query = `select * from cliente`;

          try{
            connect.query(query, function(err, data){
              if(err){
                console.log(err);
                res.status(500).json({error: "Usuarios nao encontrados no banco"});
                return;
              }
              let clientes = data;
              console.log("Consulta realizada com sucesso");
              res.status(201).json({clientes});
              
            });
           }catch(error){
            console.error("Erro ao executar a consulta: ", error);
            res.status(500).json({error: "Erro interno do servidor"})
           }
        }


        static async getAllClientes2(req, res) {
          // Extrair parametros da consulta da url
          const{filtro, ordenacao, ordem} = req.query;
          
          // Consulta sql base
          let query = `select * from cliente`

          // Clausula where

          if(filtro){
            // query = query + filtro;  -- incorreto
            //query = query + ` where ${filtro}`; // Correto, mas tem melhor
            query += ` where ${filtro}`; 
          }

          //Adicionar a clausula order by, quando houver
          if(ordenacao){
            query += ` order by ${ordenacao}`;

            //Adicionar a ordem do order by (asc ou desc)
            if(ordem){
              query += ` ${ordem}`;
            }
          }


          try{
            connect.query(query, function(err, result){
              if(err){
                console.log(err);
                res.status(500).json({error: "Usuarios nao encontrados no banco"});
                return;
              }
              console.log("Consulta realizada com sucesso");
              res.status(201).json({result});
              
            });
           }catch(error){
            console.error("Erro ao executar a consulta: ", error);
            res.status(500).json({error: "Erro interno do servidor"})
           }
          
        }

    };